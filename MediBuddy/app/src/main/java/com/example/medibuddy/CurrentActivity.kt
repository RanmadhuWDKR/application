package com.example.medibuddy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_current.*

class CurrentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler)

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

//        val bundle: Bundle? = intent.extras
//        val string: String? = intent.getStringExtra("MEDICINE_LIST")
//        val medicineArray: ArrayList<Medicine>? = intent.getParcelableArrayListExtra("MEDICINE_LIST")

        //Log.d("KKKK",medicineArray.toString())

        val dbUserData = DBUserData(this)

        medArray = dbUserData.fetchAllData()
        Log.d("KKKK", medArray.toString())

        if (!medArray.isNullOrEmpty()) {
            val adapter = MedicineAdapter(medArray)
            recyclerView.adapter = adapter
            //adapter.notifyDataSetChanged()
        }

        fab_add.setOnClickListener{
            val intent = Intent(this, ReminderActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    companion object {
        var medArray = ArrayList<Medicine>()
    }

}

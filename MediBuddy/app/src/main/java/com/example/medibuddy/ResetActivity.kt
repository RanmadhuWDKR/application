package com.example.medibuddy

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_reset.*

class ResetActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset)
        auth = Firebase.auth

        button_reset.setOnClickListener {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)

            if (email_field_reset.text.toString().isNullOrBlank()) {
                textViewResponse5.text = "E mail address is not provided."
                textViewResponse6.text = ""
            } else {
                auth.sendPasswordResetEmail(email_field_reset.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            val user = auth.currentUser
                            textViewResponse5.text = ""
                            textViewResponse6.text = "Please check your E mail for reset the password."
                        } else {
                            textViewResponse5.text = "Password Reset E mail cannot be send."
                            textViewResponse6.text = ""
                        }
                    }
            }
        }

        button_back_to_login.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}

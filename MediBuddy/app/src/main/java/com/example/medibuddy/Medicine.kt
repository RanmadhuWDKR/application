package com.example.medibuddy

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Medicine (
    val medicineId: Int?,
    val medicine_name:String,
    val medicine_quantity:String,
    val start_date:String,
    val end_date:String,
    val alarm_time:String,
    val before_meal:String
):Parcelable
package com.example.medibuddy

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_reset.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = Firebase.auth

        val button = findViewById<Button>(R.id.button_sign_up)
        button.setOnClickListener {
            signUpUser()
        }

        button_2.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun signUpUser() {
        if(email_field_sign_up.text.toString().isEmpty()) {
            textViewResponse2.text = "Enter your E mail."
            email_field_sign_up.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email_field_sign_up.text.toString()).matches()) {
            textViewResponse2.text = "Please enter a valid E mail."
            email_field_sign_up.requestFocus()
            return
        }

        if(password_field_sign_up.text.toString().isEmpty()) {
            textViewResponse2.text = "Enter a password with at least 6 characters."
            password_field_sign_up.requestFocus()
            return
        }

        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)

        auth.createUserWithEmailAndPassword(email_field_sign_up.text.toString(), password_field_sign_up.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    auth.currentUser?.sendEmailVerification()?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            textViewResponse4.text = "Sign Up Successful."
                            textViewResponse2.text = ""
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                            //val user = auth.currentUser
                            //updateUI(user, email_field_sign_up.text.toString())
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                baseContext,
                                "User is already signed up, or password does not contain at least 6 valid characters.",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
            }
    }

    /*private fun updateUI(currentUser: FirebaseUser?, emailAdd: String) {
        if(currentUser != null){
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("emailAddress",emailAdd)
            startActivity(intent)
        }
    }*/
}

package com.example.medibuddy

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQueryBuilder
import android.util.Log
import android.widget.TextView
import com.example.medibuddy.CurrentActivity.Companion.medArray
import java.util.*

object Utils {

    fun setAlarm(context: Context, timeOfAlarm: Long) {

        // Intent to start the Broadcast Receiver
        val broadcastIntent = Intent(context
            , NotificationReceiver::class.java)

        val pIntent = PendingIntent.getBroadcast(
            context,
            0,
            broadcastIntent,
            0
        )

        // Setting up AlarmManager
        val alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if (System.currentTimeMillis() < timeOfAlarm) {
            alarmMgr.set(
                AlarmManager.RTC_WAKEUP,
                timeOfAlarm,
                pIntent
            )
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeOfAlarm, AlarmManager.INTERVAL_DAY, pIntent)
        }
    }
}
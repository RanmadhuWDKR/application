package com.example.medibuddy

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.util.Log

class DBUserData(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "Alarms.db"
        val TABLE_NAME = "Alarm_log"
        val COLUMN_ID = "CID"
        val COLUMN_MEDICINE = "m_name"
        val COLUMN_QUANTITY = "m_quantity"
        val COLUMN_START_DATE = "s_date"
        val COLUMN_END_DATE = "e_date"
        val COLUMN_ALARM_TIME = "a_time"
        val COLUMN_BEFORE_OR_AFTER_MEAL = "boa_meal"
    }

    val create_db_table: String = "create table $TABLE_NAME(" +
            "$COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "$COLUMN_MEDICINE TEXT , " +
            "$COLUMN_QUANTITY TEXT," +
            "$COLUMN_START_DATE TEXT ," +
            "$COLUMN_END_DATE TEXT," +
            "$COLUMN_ALARM_TIME TEXT," +
            "$COLUMN_BEFORE_OR_AFTER_MEAL TEXT)"

    private val SQL_UPDATE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_NAME"
    var sqlObj: SQLiteDatabase = this.writableDatabase

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(create_db_table)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL(SQL_UPDATE_ENTRIES)
        onCreate(db!!)
    }

    fun insertUserDetails(medicine: Medicine): Boolean {
        val values = ContentValues()
        values.put(COLUMN_MEDICINE, medicine.medicine_name)
        values.put(COLUMN_QUANTITY, medicine.medicine_quantity)
        values.put(COLUMN_START_DATE, medicine.start_date)
        values.put(COLUMN_END_DATE, medicine.end_date)
        values.put(COLUMN_ALARM_TIME, medicine.alarm_time)
        values.put(COLUMN_BEFORE_OR_AFTER_MEAL, medicine.before_meal)
        //values.put(COLUMN_COUNTDOWN, medicine.time_range)

        val db = this.writableDatabase
        db.insert(TABLE_NAME, null, values)
        db.close()

        return true
    }

    fun fetchAllData(): ArrayList<Medicine> {
        val arraylist = ArrayList<Medicine>()
        val sqb = SQLiteQueryBuilder()
        sqb.tables = TABLE_NAME
        //val selection = "$COLUMN_ID =?"
        val cols = arrayOf("CID","m_name","m_quantity", "s_date", "e_date", "a_time", "boa_meal")
        //val rowSelArg = arrayOf(keyword)
        val cur = sqb.query(sqlObj, cols, null, null, null, null, "1")
        if (cur.moveToFirst()) {
            do {
                val mid = cur.getInt(cur.getColumnIndex("CID"))
                val m_name = cur.getString(cur.getColumnIndex("m_name"))
                val m_quantity = cur.getString(cur.getColumnIndex("m_quantity"))
                val s_date = cur.getString(cur.getColumnIndex("s_date"))
                val e_date = cur.getString(cur.getColumnIndex("e_date"))
                val a_time = cur.getString(cur.getColumnIndex("a_time"))
                val boa_meal = cur.getString(cur.getColumnIndex("boa_meal"))
                //val t_range = cur.getString(cur.getColumnIndex("t_range"))
                arraylist.add(Medicine(medicineId = mid, medicine_name = m_name, medicine_quantity = m_quantity, start_date = s_date, end_date = e_date,alarm_time = a_time, before_meal = boa_meal))
            } while (cur.moveToNext())
        }
        var count: Int = arraylist.size
        Log.d("DDDD",arraylist.toString())
        return arraylist
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteAlarm(cid: String): Boolean {
// Gets the data repository in write mode
        val db = writableDatabase
// Define 'where' part of query.
        val selection = "$COLUMN_ID LIKE ?"
// Specify arguments in placeholder order.
        val selectionArgs = arrayOf(cid)
// Issue SQL statement.
        db.delete(TABLE_NAME, selection, selectionArgs)
        return true
    }

}


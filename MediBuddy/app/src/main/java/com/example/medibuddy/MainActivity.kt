package com.example.medibuddy

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = Firebase.auth

        button_login.setOnClickListener {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)

            if (email_field_login.text.toString().isNullOrEmpty() || password_field_login.text.toString().isNullOrEmpty()) {
                textViewResponse.text = "E mail Address or Password is not provided"
            } else {
                auth.signInWithEmailAndPassword(email_field_login.text.toString(), password_field_login.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            textViewResponse3.text = "Sign In Successful."
                            textViewResponse.text = ""
                            val user = auth.currentUser
                            updateUI(user, email_field_login.text.toString())
                        } else {
                            textViewResponse.text = "Invalid E mail or Password."
                        }
                    }
            }
        }

        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener{
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        val button2 = findViewById<Button>(R.id.button_reset)
        button2.setOnClickListener{
            val intent = Intent(this, ResetActivity::class.java)
            startActivity(intent)
        }
    }


    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        val em = currentUser?.email.toString()
        updateUI(currentUser, em)
    }

    private fun updateUI(currentUser: FirebaseUser?, emailAdd: String) {
        if(currentUser != null) {
            if (currentUser.isEmailVerified) {
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra("emailAddress", emailAdd)
                startActivity(intent)
                finish()
            } else {
                textViewResponse.text = "Please verify your E mail to proceed."
                textViewResponse3.text = ""
            }
        }
    }
}
